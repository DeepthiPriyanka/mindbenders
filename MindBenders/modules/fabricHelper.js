var options = { 
"httpRequestOptions": { 
"enableBackgroundTransfer": true // new optional param to enable BG transfer for a particular operation 
} 
}; 

function commonMFIntegServiceCall(serviceName, operationName, headers, inputParams, callerSuccessCallBack, callerFailureCallBack) 
{
  var konyObject = kony.sdk.getCurrentInstance();
  kony.print("commonMFServiceCall for operationName"+ operationName + "< with inputParams>" + JSON.stringify(inputParams));
  
  try
  {
      var integrationObject = konyObject.getIntegrationService(serviceName);
      integrationObject.invokeOperation(operationName, headers, inputParams,
                                        successCallBack, failureCallBack, options);  
    
  }
  catch(e){
    alert("Exception Message::"+e);
    callerFailureCallBack({errorMsg : e});
  }

    function successCallBack(resultset) {
  		callerSuccessCallBack(resultset);
  	}

  	function failureCallBack(resultset) {
  		kony.print("failure callback"+ JSON.stringify(resultset));
    	//delegate to the callers callback
        if(null !== resultset && resultset.hasOwnProperty("opstatus")){
           var opstatus = resultset.opstatus;
   		   if(undefined !== opstatus && null !== opstatus && ("1011" == opstatus|| 1011 == opstatus)){
             kony.ui.Alert({
                message: "Could not connect to our Networks at this time. Please try again later.",
                alertType: constants.ALERT_TYPE_INFO,
                alertTitle: "Information",
                yesLabel:"OK",
                noLabel: "",
                alertIcon: "transparent.png",
                alertHandler: {}
              },{}
              );
           }else{
             callerFailureCallBack(resultset);
           }
        }else{
          callerFailureCallBack(resultset);
        }
  	} 
}