var gblStoreKey = null;

function generetaKey(){
  var encryptDecryptKey = kony.crypto.newKey("securerandom",256, {
    subalgo: "aes"
  });
  kony.print("@@ store encryptDecryptKey :: "+encryptDecryptKey);
  var myUniqueIDKey = kony.crypto.saveKey("encryptionKey", encryptDecryptKey); 
  kony.print("@@ store myUniqueIDKey :: "+myUniqueIDKey);
  kony.store.setItem("gblStoreKey",myUniqueIDKey); 

}

function encryptText(inputString){
  kony.print("@@ store :: "+inputString);
  var gblStoreKeyValue = kony.store.getItem("gblStoreKey");  
  var myUniqueKey = kony.crypto.readKey(gblStoreKeyValue); 
  kony.print("@@ store myUniqueKey :: "+myUniqueKey);
  var encryptedData = kony.crypto.encrypt("aes", myUniqueKey,inputString, {padding:"pkcs5",mode:"cbc",initializationvector:
                                                                           "1234567890123456"});
  return encryptedData;
}

function setItemIntoStore(deviceId,accountNumber,isAlexaEnabled){
  var key = "reqKey";
  var data = [{"deviceId":deviceId,"accountNumber":accountNumber,"isAlexaEnabled":isAlexaEnabled}]; 
  var encText = encryptText(JSON.stringify(data));
  kony.print("@@ store encText ::"+encText);
  var encAccess = { AccessId : encText } ;                                                                                                                
  kony.store.setItem(key,[ encAccess ]);
}

function getItemIntoStore(){
  var decText = "";
  var decTextArr = "";
  var readDeviceTable = kony.store.getItem("reqKey");
  alert("@@ store getItemIntoStore readDeviceTable : "+readDeviceTable);
  if((readDeviceTable != null)){
    var cipherUID = readDeviceTable[ 0 ][ ("AccessId") ];
    decText = decryptText(cipherUID);
    decTextArr = JSON.parse(decText);
    alert("@@ store getItemIntoStore decText1 : "+JSON.stringify(decTextArr));
    return decTextArr;
    
  }
  return JSON.stringify(decTextArr);
}

function decryptText(encryptedString){
 // encryptedString = kony.convertToRawBytes(encryptedString);
  kony.print("@@ store decryptText encryptedString : "+ encryptedString);
  var gblStoreKeyValue = kony.store.getItem("gblStoreKey");
  kony.print("@@ store gblStoreKeyValue :: "+gblStoreKeyValue);
  var myUniqueKey = kony.crypto.readKey(gblStoreKeyValue); 
  kony.print("@@ store myUniqueKey :: "+myUniqueKey);
  return kony.crypto.decrypt("aes", 
                             myUniqueKey, 
                             encryptedString, 
                             {padding:"pkcs5",mode:"cbc",initializationvector:"1234567890123456"});
}