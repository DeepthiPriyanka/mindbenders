define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for headerButtonLeft **/
    AS_Button_bf11e4a060fb455b9d37457dd87fa360: function AS_Button_bf11e4a060fb455b9d37457dd87fa360(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmAccounts");
        ntf.navigate();
    },
    /** onClick defined for headerButtonRight **/
    AS_Button_d21d18eb61fe4dc88bca98b624e92790: function AS_Button_d21d18eb61fe4dc88bca98b624e92790(eventobject) {
        var self = this;
        this.view.flxMainPopup.setVisibility(true);
    },
    /** onClick defined for btnTogal **/
    AS_Button_e88f6370e4f244b782bbcb49e7f75ae7: function AS_Button_e88f6370e4f244b782bbcb49e7f75ae7(eventobject) {
        var self = this;
        if (this.view.btnTogal.skin == "active") {
            this.view.btnTogal.skin = "inactive";
            this.view.lstAccountNumbers.setVisibility(false);
            this.view.btnSave.setVisibility(false);
        } else {
            this.view.btnTogal.skin = "active";
            this.view.lstAccountNumbers.setVisibility(true);
            this.view.btnSave.setVisibility(true);
        }
    },
    /** onClick defined for btnSave **/
    AS_Button_d0cf1e24b44e4ed5a1aae12cd2838cb7: function AS_Button_d0cf1e24b44e4ed5a1aae12cd2838cb7(eventobject) {
        var self = this;
        this.onClickSave();
    },
    /** onClick defined for flxNo **/
    AS_FlexContainer_b88f4a0221944684a201ddecd255f668: function AS_FlexContainer_b88f4a0221944684a201ddecd255f668(eventobject) {
        var self = this;
        this.view.flxMainPopup.setVisibility(false);
    },
    /** onClick defined for flxYes **/
    AS_FlexContainer_a3457cc4b0954667882ba0336186081e: function AS_FlexContainer_a3457cc4b0954667882ba0336186081e(eventobject) {
        var self = this;
        this.view.flxMainPopup.setVisibility(false);
        logout();
    },
    /** preShow defined for frmSettings **/
    AS_Form_a70af38275304f9ab015b3e7d32568db: function AS_Form_a70af38275304f9ab015b3e7d32568db(eventobject) {
        var self = this;
        return self.onPreshow.call(this);
    }
});