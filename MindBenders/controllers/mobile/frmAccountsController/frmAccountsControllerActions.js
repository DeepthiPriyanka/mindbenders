define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for headerButtonLeft **/
    AS_Button_e45808ff7c8441e4aadf0750bef3e0ee: function AS_Button_e45808ff7c8441e4aadf0750bef3e0ee(eventobject) {
        var self = this;
        this.navigateToSettings();
    },
    /** onClick defined for headerButtonRight **/
    AS_Button_af07f2a92bd7441896981b52e6da352f: function AS_Button_af07f2a92bd7441896981b52e6da352f(eventobject) {
        var self = this;
        this.view.flxMainPopup.setVisibility(true);
    },
    /** onClick defined for flxNo **/
    AS_FlexContainer_aa23afceec924b10b212f04915e73e0f: function AS_FlexContainer_aa23afceec924b10b212f04915e73e0f(eventobject) {
        var self = this;
        this.view.flxMainPopup.setVisibility(false);
    },
    /** onClick defined for flxYes **/
    AS_FlexContainer_g58a7e88cf5542cf84b05ae361cb76b5: function AS_FlexContainer_g58a7e88cf5542cf84b05ae361cb76b5(eventobject) {
        var self = this;
        this.view.segAccounts.removeAll();
        this.view.flxMainPopup.setVisibility(false);
        logout();
    },
    /** onClick defined for Button0cba41816481243 **/
    AS_Button_ga866ba50a284b0f90d1dcc33a88fb5f: function AS_Button_ga866ba50a284b0f90d1dcc33a88fb5f(eventobject) {
        var self = this;
        if (this.view.Button0cba41816481243.skin == "active") this.view.Button0cba41816481243.skin = "inactive"
        else this.view.Button0cba41816481243.skin = "active"
    }
});