define({ 

  onNavigate : function ()
  {
    this.view.segAccounts.removeAll();
    this.bindEvents();
  }, 
  bindEvents : function()
  {
    //#ifdef android
    this.view.onDeviceBack=this.doNothing;
    //#endif
    this.getAccounts();
  },

  doNothing : function(){
    //do nothing
  },

  getAccounts : function(){
    try{
      showLoadingScreen();
      var serviceName = "Accounts";
      var operationName = "getAccounts";
      //kony.print("inputparams : " + JSON.stringify(inputparams));
      commonMFIntegServiceCall(serviceName,operationName, {},{}, 
                               this.getAccountsSuccessCallback,                     
                               this.getAccountsFailureCallback
                              );
    }catch(e){
      kony.print("Exception in getAccounts : " + e);
    }
  },
  getAccountsFailureCallback : function(error){
    dismissLoadingScreen();
    kony.print("Deepu failure : "+JSON.stringify(error));
  },
  getAccountsSuccessCallback : function(response){
    //kony.print("Deepu response : "+JSON.stringify(response));
    var count = 1;
    var accountsData = [];
    //accountsData = dummyData; //response.body;
    for(var i=0;i<dummyData.length;i++){
      var segData = {
        "availableBalance":"Available Balance : "+dummyData[i].availableBalance+dummyData[i].currencyId,
        "productName":"Account Type : "+dummyData[i].productName,
        "displayName":dummyData[i].displayName,
        "accountSkinColor":{"skin":this.applySkin(count)}
      };
      if(count === 5){
        count = 0;
      }
      accountsData.push(segData);
      count++;
    }
    this.view.segAccounts.widgetDataMap = {
      "lblAccountType" : "displayName",
      "lblAvailable" : "availableBalance",
      "accountId":"accountId",
      "lblProduct" : "productName",
      "lblArrow" : "arrow",
      "flxAccountType":"accountSkinColor",

    };
    this.view.segAccounts.setData(accountsData);
    dismissLoadingScreen();
  },
  applySkin : function(count){
    var accSkin = "";
    switch(count){
      case 1 : {
        accSkin = "sknPersonalAcc";
        break;
      }
      case 2 : {
        accSkin = "sknBusinessAcc";
        break;
      }
      case 3 : {
        accSkin = "sknHouseAcc";
        break;
      }
      case 4 : {
        accSkin = "sknCreditCardAcc";
        break;
      }
      case 5 : {
        accSkin = "sknJordanSavingsAcc";
        break;
      }
      default : {
        accSkin = "sknJordanSavingsAcc";
      }
    }
    return accSkin;
  },
  navigateToSettings :  function(){
    var data = dummyData;
    //alert("data :::"+JSON.stringify(data));
    var accountData = [];
    accountData.push(["lb1","Select one account"]);
    for(var i = 0;i<data.length;i++){
      var temp= [data[i].accountId,data[i].displayName];
      accountData.push(temp);
    }
    // alert("accountData::"+JSON.stringify(accountData));
    gblAccountsData = accountData;
    //this.view.lstAccountNumbers.masterData = accountData;
    new kony.mvc.Navigation("frmSettings").navigate();
  }

});


