define({ 

  onNavigate : function (navigationParams)
  {
    this.bindEvents();
  }, 

  bindEvents : function()
  {
    //#ifdef android
    this.view.onDeviceBack=this.doNothing;
    //#endif
    this.view.btnSignin.onClick = this.showAccounts;
  },

  doNothing : function(){
    //do nothing
  },
  
  showAccounts : function(){
    kony.application.showLoadingScreen();
     new kony.mvc.Navigation("frmAccounts").navigate();
  }

});